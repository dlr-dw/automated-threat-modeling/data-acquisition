# Threat Analysis in Open-Source Projects

This repository is associated with the paper titled "Finding a Needle in a Haystack: Threat Analysis in Open-Source Projects," scheduled for presentation at the Saner Workshop on Mining Software Repositories for Security & Privacy (MSR4S&P) in 2024. The paper explores the prevalence of threat analysis in open-source projects on GitHub. The repository includes scripts for searching GitHub, filtering the identified repositories, and analyzing the results. Additionally, it contains lists of search results that form the basis for the findings presented in the paper.

## Content

* *processed_repo_lists:* folder contains the extended and filtered search results per tool and the manually filtered Python repository list
* *notebooks:* folder contains Jupyter notebooks to search for repositories, filter and evaluate them

## How to use

1. Install requirements `pip -r requirements.txt`
2. Create a repository list with *create_repos_lists.ipynb*
   1. Search on Github for repositories with threat models
   2. Extend search results with additional information (stars, #files, etc.)
3. Filter the repository lists with *filter_repo_list.ipynb*
4. Evaluate the repository lists with *evaluation.ipynb*
   1. Get the most common topics
   2. Get some characteristics of the repositories (stars, #files, etc.)
   3. Get a distribution of languages or/and tools over the repository lists

## Cite

```bibtex
@INPROCEEDINGS {10621728,
author = {B. Gruner and S. Heckner and T. Sonnekalb and B. Bouhlal and C. Brust},
booktitle = {2024 IEEE International Conference on Software Analysis, Evolution and Reengineering - Companion (SANER-C)},
title = {Finding a Needle in a Haystack: Threat Analysis in Open-Source Projects},
year = {2024},
volume = {},
issn = {},
pages = {141-145},
doi = {10.1109/SANER-C62648.2024.00024},
url = {https://doi.ieeecomputersociety.org/10.1109/SANER-C62648.2024.00024},
publisher = {IEEE Computer Society},
address = {Los Alamitos, CA, USA},
month = {mar}
}

```

## License

Please refer to the `LICENSE` file for further information about how the content is licensed.